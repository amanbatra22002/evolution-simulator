# Evolution Simulator



## Introduction

In my project, the Evolution Simulator, I've created a virtual playground where square creatures navigate a simple world. These creatures have brains called neural networks that help them decide whether to move left, right, forward, or backward. I built this using Unity 3D, a game development tool.

The simulation happens in short 10-second rounds, during which I check how close each creature is to safe areas. I decide how well a creature is doing based on whether it's in a safe spot or not. The magic happens through a genetic algorithm, a kind of behind-the-scenes process inspired by nature. I select the best creatures, mix their "genetic" information, and introduce small changes for diversity.

Each iteration these creatures are spawned at random locations. When a creature reaches the safe zone they are given the permission to reproduce. In the start of the next generation they will die and their offsprings will spawn. The idea is to visualize how evolution favours the stronger genes.

As these creatures go through many rounds, their decision-making gets better, mirroring how animals adapt in the real world. My project aims to show how this process works visually, providing a simple and engaging way to understand concepts like neural networks and evolution.

Looking forward, I might make small adjustments for better performance, add a few more things for creatures to do, or maybe show some interesting data as the simulation runs. My Evolution Simulator is a humble attempt to make complex ideas about brains and evolution easy to grasp and fun to explore.


## Some Visualisations

![Safe Zones](zones.png)

In the above image, green ones are the safe zone. We will make them transparent during the simulation.


Simulation:

[![Click to redirect](https://img.youtube.com/vi/WX-XuabJ7o4/0.jpg)](https://www.youtube.com/watch?v=WX-XuabJ7o4)


## Tools Required

- [Unity Engine v6000.0.32f1](https://unity.com/download)
- [VS Code](https://code.visualstudio.com/) or [Visual Studio Community](https://visualstudio.microsoft.com/vs/community/)
- [.Net SDK](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks)
- Extension For VS Code:
    - [C# Dev Kit](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csdevkit)

## Algorithms
- Neural Networks
- Genetic Algorithm

## Installation

To open the source code:

1. Download and Install [Git](https://git-scm.com/)

2. Download this repository with:
```
git clone https://gitlab.com/aman-batra-dev/evolution-simulator.git
```

3. Open Unity

4. Open project folder into unity

5. Voila! play around as you wish


## References
I took inspiration for idea from: [This Video](https://www.youtube.com/watch?v=N3tRFayqVtk&t=2s)

## Credits
Made By:
[Aman Batra](https://gitlab.com/aman-batra-dev)
