using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class Brain : MonoBehaviour
{
    Transform enemyTransform;
    public int killed=0;
    public NN Network= new NN(NN.Learn.LearnType.Unsupervised);
    public NN killNetwork = new NN(NN.Learn.LearnType.Unsupervised);
    [SerializeField]
    float x,xmin;
    [SerializeField]
    float y;
    public float fitness=0;
    bool CanKill=false;
    public Color32 color;
    [SerializeField]
    List<float> result;
    [SerializeField]
    float isKill=0;
    Dictionary<string,float> probs = new Dictionary<string, float>();
    float speed =20f;
    bool CanMove = true;
    void Start()
    {
        
    }
    public void XY_Brain(List<float> ws=null)
    {
        NN.Layer inputLayer = new NN.Layer("xy_input",2,NN.Layer.LayerTypeEnum.hidden,NN.Layer.ActivationFunctionEnum.Identity);
        NN.Layer hiddenLayer1 = new NN.Layer("xy_hidden1",32,NN.Layer.LayerTypeEnum.hidden,NN.Layer.ActivationFunctionEnum.LeakyReLU);
        NN.Layer hiddenLayer2 = new NN.Layer("xy_hidden2",16,NN.Layer.LayerTypeEnum.hidden,NN.Layer.ActivationFunctionEnum.BipolarSigmoid);
        NN.Layer outputLayer = new NN.Layer("xy_hidden3",3,NN.Layer.LayerTypeEnum.output,NN.Layer.ActivationFunctionEnum.BipolarSigmoid);
        Network.AddLayer(inputLayer);
        Network.AddLayer(hiddenLayer1);
        Network.AddLayer(hiddenLayer2);
        Network.AddLayer(outputLayer);
        Network.ConnectLayers(ws);
        result = Network.ForwardPropogation(new List<float>{transform.position.x/55,transform.position.y/55});
        x = result[0];
        xmin  = result[1]*55;
        y = result[2];
    }
    void Movement()
    {
        Vector2 dir = new Vector2(x,y);
        GetComponent<Rigidbody2D>().linearVelocity = dir.normalized*xmin*speed*Time.fixedDeltaTime;
    }
    void ShouldKill()
    {
        if(CanKill)
        {
            CanKill=false;
            isKill = Network.ForwardPropogation(new List<float>{enemyTransform.position.x,enemyTransform.position.y})[0];
            float chance = UnityEngine.Random.Range(0.000f,1.000f);
            if(isKill>=0.9)
            {
                Destroy(enemyTransform.gameObject);
                killed+=1;
            }
        }
    }
    public void SetColour()
    {
        color = new Color32((byte)Random.Range(0,226),(byte)Random.Range(0,226),(byte)Random.Range(0,226),(byte)225);
        GetComponent<SpriteRenderer>().color = color;
    }
    void Update()
    {
        GetComponent<SpriteRenderer>().color = color;
        //ShouldKill();
    }
    void FixedUpdate()
    {
        if(CanMove)
            Movement();
    }
    public void KillBrain(List<float> ws=null)
    {
        NN.Layer input = new NN.Layer("KillInput",2,NN.Layer.LayerTypeEnum.hidden,NN.Layer.ActivationFunctionEnum.BipolarSigmoid);
        NN.Layer hiddenlayer = new NN.Layer("KillHidden",25,NN.Layer.LayerTypeEnum.hidden,NN.Layer.ActivationFunctionEnum.Sigmoid);
        NN.Layer hiddenLayer2 = new NN.Layer("KillHidden2",5,NN.Layer.LayerTypeEnum.hidden,NN.Layer.ActivationFunctionEnum.Sigmoid);
        NN.Layer outputLayer = new NN.Layer("KillOutput",1,NN.Layer.LayerTypeEnum.output,NN.Layer.ActivationFunctionEnum.Sigmoid);
        killNetwork.AddLayer(input);
        killNetwork.AddLayer(hiddenlayer);
        killNetwork.AddLayer(hiddenLayer2);
        killNetwork.AddLayer(outputLayer);
        killNetwork.ConnectLayers(ws);
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "selection")
        {
            fitness = 1;
        }
        
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag=="Walls")
        {
            CanMove = false;
            GetComponent<Rigidbody2D>().linearVelocity = Vector2.zero;
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "selection")
        {
            fitness=1;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "selection")
        {
            fitness = 0;
        }
    }
    
}
