using System.Collections;
using System.Collections.Generic;
using System;
public class NN
{
    public List<Layer> LayerList = new List<Layer>();
    public Learn learning;
    public NN(Learn.LearnType learn)
    {
        learning = new Learn(learn);
    }

    public class Layer
    {
        string LayerName;
        public enum LayerTypeEnum{input,hidden,output};
        LayerTypeEnum LayerType = new LayerTypeEnum();
        public List<Node> Nodes = new List<Node>();
        public enum ActivationFunctionEnum{Sigmoid,ReLU,Identity,LeakyReLU,BipolarSigmoid,Softmax}
        ActivationFunctionEnum Act = new ActivationFunctionEnum();
        public Layer(string name,int size,LayerTypeEnum l,ActivationFunctionEnum a)
        {
            LayerName = name;
            LayerType = l;
            Act =a;
            for(int i=0;i<size;i++)
            {
                Nodes.Add(new Node(LayerType,Act));
            }
        }
        public LayerTypeEnum GetLayerType()
        {
            return LayerType;
        }
        public class Node
        {
            float ExpectedOutput,ActualOutput;
            float sigma;
            float delta;
            List<float> inputBuffer= new List<float>();
            List<float> IntermediateOutputs = new List<float>();
            List<Node> PreviousNodes = new List<Node>();
            float output;
            LayerTypeEnum LayerType = new LayerTypeEnum();
            ActivationFunctionEnum activation = new ActivationFunctionEnum();
            List<Node> ConnectedNodes = new List<Node>();
            
            public List<float> ConnectedWeights = new List<float>();
            public Node(LayerTypeEnum l,ActivationFunctionEnum a)
            {
                LayerType = l;
                activation = a;
            }
            
            public void ConnectNode(Node n,float weight)
            {
                ConnectedNodes.Add(n);
                ConnectedWeights.Add(weight);
            }
            public void SetInput(float input)
            {
                inputBuffer.Add(input);
            }
            void Submission()
            {
                sigma = 0;
                foreach(float input in inputBuffer)
                {
                    sigma += input;
                }
                inputBuffer.Clear();
            }
            public List<float> Softmax()
            {
                if(activation==ActivationFunctionEnum.Softmax)
                {
                    sigma = 0;
                    List<float> list = new List<float>();
                    foreach(float input in IntermediateOutputs)
                    {
                        sigma += MathF.Exp(input);
                    }
                    foreach(float input in IntermediateOutputs)
                    {
                        list.Add(MathF.Exp(input)/sigma);
                    }
                    return list;
                }
                return null;
            }
            float Apply_ActivationFunction()
            {
                if(activation == ActivationFunctionEnum.Sigmoid)
                {
                    
                    return (1/(1+MathF.Exp((-1*sigma))));
                }
                else if(activation == ActivationFunctionEnum.Identity)
                {
                    return (sigma);
                }
                else if(activation==ActivationFunctionEnum.ReLU)
                {
                    return (MathF.Max(0,sigma));
                }
                else if(activation==ActivationFunctionEnum.LeakyReLU)
                {
                    return (MathF.Max(0.01f*sigma,sigma));
                }
                else if(activation==ActivationFunctionEnum.BipolarSigmoid)
                {
                    return ((1-MathF.Exp((-1*sigma)))/(1+MathF.Exp((-1*sigma))));
                }
                else if(activation==ActivationFunctionEnum.Softmax)
                {
                    return 0;
                }
                else
                {
                    return (0);
                }
                
            }
            public float Forward()
            {
                Submission();
                output = Apply_ActivationFunction();
                foreach(Node n in ConnectedNodes)
                {
                    n.inputBuffer.Add(output*ConnectedWeights[ConnectedNodes.IndexOf(n)]);
                    n.IntermediateOutputs.Add(output);
                    n.PreviousNodes.Add(this);
                }
                return output;
            }

            public void SetBackParams(float Eput,float Aput)
            {
                if(LayerType==LayerTypeEnum.output)
                {
                    ExpectedOutput = Eput;
                    ActualOutput = Aput;
                }
            }
            public void Backward()
            {
                if(activation==ActivationFunctionEnum.BipolarSigmoid)
                {
                    BipolarSigmoidBack();
                }
                if(activation==ActivationFunctionEnum.Sigmoid)
                {
                    SigmoidBack();
                }
            }

            void SigmoidBack()
            {
                float dW;
                float steepness=1f;
                float learningRate = 0.01f;
                if(LayerType==LayerTypeEnum.input)
                {
                    return;
                }
                if(LayerType==Layer.LayerTypeEnum.output)
                {
                    delta = (ExpectedOutput-ActualOutput)*steepness*output*(1-output);
                    for(int i=0;i<IntermediateOutputs.Count;i++)
                    {
                        dW = learningRate*delta*IntermediateOutputs[i];
                        PreviousNodes[i].ConnectedWeights[PreviousNodes[i].ConnectedNodes.IndexOf(this)] += dW;
                    }
                }
                if(LayerType==LayerTypeEnum.hidden)
                {
                    for(int i=0;i<ConnectedNodes.Count;i++)
                    {
                        delta += ConnectedNodes[i].delta*ConnectedWeights[i];
                    }
                    delta = delta*steepness*sigma*(1-sigma);
                    for(int i=0;i<IntermediateOutputs.Count;i++)
                    {
                        dW = learningRate*delta*IntermediateOutputs[i];
                        PreviousNodes[i].ConnectedWeights[PreviousNodes[i].ConnectedNodes.IndexOf(this)] += dW;
                    }
                }
            }
            void BipolarSigmoidBack()
            {
                float dW;
                float steepness=0.5f;
                float learningRate = 0.01f;
                if(LayerType==LayerTypeEnum.input)
                {
                    return;
                }
                if(LayerType==Layer.LayerTypeEnum.output)
                {
                    delta = (ExpectedOutput-ActualOutput)*steepness*(1-output)*(1+output);
                    for(int i=0;i<IntermediateOutputs.Count;i++)
                    {
                        dW = learningRate*delta*IntermediateOutputs[i];
                        PreviousNodes[i].ConnectedWeights[PreviousNodes[i].ConnectedNodes.IndexOf(this)] += dW;
                    }
                }
                if(LayerType==LayerTypeEnum.hidden)
                {
                    for(int i=0;i<ConnectedNodes.Count;i++)
                    {
                        delta += ConnectedNodes[i].delta*ConnectedWeights[i];
                    }
                    delta = delta*steepness*(1-sigma)*(1+sigma);
                    for(int i=0;i<IntermediateOutputs.Count;i++)
                    {
                        dW = learningRate*delta*IntermediateOutputs[i];
                        PreviousNodes[i].ConnectedWeights[PreviousNodes[i].ConnectedNodes.IndexOf(this)] += dW;
                    }
                }
            }
        }
    }

    public class Learn
    {
        public enum LearnType{Supervised,Unsupervised,Reinforcement};
        LearnType LearnT;
        public Learn(LearnType learningType)
        {
            LearnT = learningType;
        }
        public LearnType GetLearnType()
        {
            return LearnT;
        }
    }
    public void AddLayer(Layer l)
    {
        LayerList.Add(l);
    }
    public void ConnectLayers(List<float> weights=null)
    {
        for(int i=0;i<LayerList.Count-1;i++)
        {
            foreach(Layer.Node current in LayerList[i].Nodes)
            {
                foreach(Layer.Node next in LayerList[i+1].Nodes)
                {
                    float weight;
                    if(weights==null)
                    {
                        Random r = new Random();
                        weight = ((float)r.Next(-100,100))/100f;
                        current.ConnectNode(next,weight);
                    }
                    else
                    {
                        current.ConnectNode(next,weights[LayerList[i+1].Nodes.IndexOf(next)]);
                    }
                }
            }    
        }
    }
    public List<float> ForwardPropogation(List<float> inputList)
    {
        float result=0;
        List<float> Final_result=new List<float>();
        for(int i=0;i<LayerList[0].Nodes.Count;i++)
        {
            LayerList[0].Nodes[i].SetInput(inputList[i]);
        }
        foreach(Layer currentLayer in LayerList)
        {
            foreach(Layer.Node currentNode in currentLayer.Nodes)
            {
                List<float> softmax = currentNode.Softmax();
                if(softmax!=null)
                {
                    return softmax;
                }
                result=currentNode.Forward();
                if(currentLayer.GetLayerType()==Layer.LayerTypeEnum.output)
                {
                    Final_result.Add(result);
                }
            }
        }
        return Final_result;
    }

    public void BackwardPropagate(List<float> ExpectedOuptut,List<float> ActualOutput)
    {
        if(learning.GetLearnType()==Learn.LearnType.Unsupervised)
        {
            return;
        }
        
        foreach(Layer currentLayer in LayerList)
        {
            foreach(Layer.Node currentNode in currentLayer.Nodes)
            {
                if(currentLayer.GetLayerType()==Layer.LayerTypeEnum.output)
                {
                    int index = currentLayer.Nodes.IndexOf(currentNode);
                    currentNode.SetBackParams(ExpectedOuptut[index],ActualOutput[index]);
                }
            }
        }
        foreach(Layer currentLayer in LayerList)
        {
            foreach(Layer.Node currentNode in currentLayer.Nodes)
            {
                currentNode.Backward();
            }
        }
    }
}