using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;
public class GA:MonoBehaviour
{
    float time=0,population=0;
    int killcounter=0,discarded,accepted=0;
    List<List<List<float>>> AllGenes = new List<List<List<float>>>();
    List<GameObject> mobs;
    bool call = true;
    public GameObject mobPrefab;
    public Dictionary<List<List<float>>,float> dict; 

    public Text GenCounter, TimerHolder,PopulationCounter,KillHolder,SurvivalCounter;
    int gen;

    void Start()
    {
        gen=1;
        GenCounter.text = "Gen: "+gen;
        mobs = GameObject.FindGameObjectsWithTag("mob").ToList<GameObject>();
        foreach(GameObject mob in mobs)
        {
            mob.GetComponent<Brain>().XY_Brain(null);
            mob.GetComponent<Brain>().SetColour();
            mob.GetComponent<Brain>().KillBrain(null);
        }
    }
    void phenotypeToGenotype(NN network,Color32 color,float fitness,NN killNetwork)
    {
        if(fitness==0)
        {
            discarded+=1;
            return;
        }
        accepted+=1;
        List<float> weights=new List<float>();
        List<List<float>> gene=new List<List<float>>();
        List<float> colorGrid=new List<float>();
        List<float> wkn = new List<float>();
        foreach(NN.Layer layer in network.LayerList)
        {
            foreach(NN.Layer.Node node in layer.Nodes)
            {
                foreach(float w in node.ConnectedWeights)
                {
                    weights.Add(w);
                }
            }
        }
        foreach(NN.Layer layer in killNetwork.LayerList)
        {
            foreach(NN.Layer.Node node in layer.Nodes)
            {
                foreach(float w in node.ConnectedWeights)
                {
                    wkn.Add(w);
                }
            }
        }
        gene.Add(weights);
        colorGrid.Add(color.r);
        colorGrid.Add(color.g);
        colorGrid.Add(color.b);
        gene.Add(colorGrid);
        gene.Add(wkn);
        AllGenes.Add(gene);
    }
    void genotypeToPhenotype(ref Brain b,ref Color32 color, List<List<float>> g)
    {
        Debug.Log("List: "+g[2].Count);
        b.XY_Brain(g[0]);
        b.KillBrain(g[2]);
        color.r= (byte)g[1][0];
        color.g= (byte)g[1][1];
        color.b= (byte)g[1][2];
        color.a = 255;
    }

    void Selection()
    {
        AllGenes = new List<List<List<float>>>();
        mobs = GameObject.FindGameObjectsWithTag("mob").ToList<GameObject>();
        foreach(GameObject mob in mobs)
        {
            Brain b  = mob.GetComponent<Brain>();
            phenotypeToGenotype(b.Network,b.color,b.fitness,b.killNetwork);
        }
        
    }
    void OnePointCrossOver()
    {
        for(int i=0;i<AllGenes.Count-1;i=i+2)
        {
            for(int j=AllGenes[i][0].Count/2;j<AllGenes[i][0].Count;j++)
            {
                float f = AllGenes[i][0][j];
                AllGenes[i][0][j] = AllGenes[i+1][0][j];
                AllGenes[i+1][0][j] = f;
            }
            for(int j=AllGenes[i][2].Count/2;j<AllGenes[i][2].Count;j++)
            {
                float f = AllGenes[i][2][j];
                AllGenes[i][2][j] = AllGenes[i+1][2][j];
                AllGenes[i+1][2][j] = f;
            }
        }
    }
    void Mutation()
    {
        AllGenes.AddRange(AllGenes);
        for(int i=AllGenes.Count-1;i>999;i--)
        {
          AllGenes.Remove(AllGenes[i]);
        }
        for(int i=0;i<AllGenes.Count;i++)
        {
            int j=0;
            int p=0;
            float chance = UnityEngine.Random.Range(0.00f,100.0f);
            while(j<AllGenes[i][0].Count*0.001)
            {
                int k = UnityEngine.Random.Range(0,AllGenes[i][0].Count);
                System.Random r = new System.Random();
                AllGenes[i][0][k] = ((float)r.Next(-100,100))/100f;
                j+=1;
            }
            while(p<AllGenes[i][2].Count*0.01)
            {
                int k = UnityEngine.Random.Range(0,AllGenes[i][2].Count);
                System.Random r = new System.Random();
                AllGenes[i][2][k] = ((float)r.Next(-2000,2000))/1000f;
                p+=1;
            }
        }
    }
    void Compile()
    {
        gen +=1;
        GenCounter.text = "Gen: "+gen;
        Selection();
        Debug.Log("Gene Count: "+AllGenes.Count);
        OnePointCrossOver();
        Mutation();
        for(int i=0;i<mobs.Count;i++)
        {
            Destroy(mobs[i]);
        }
        mobs.Clear();
        foreach(List<List<float>> gene in AllGenes)
        {
            GameObject mob = Instantiate<GameObject>(mobPrefab);
            mob.transform.position = new Vector3(UnityEngine.Random.Range(-55,55),UnityEngine.Random.Range(-55,55),-0.2f);
            Brain b = mob.GetComponent<Brain>();
            genotypeToPhenotype(ref b,ref b.color, gene);
        }
        AllGenes.Clear();
        call= true;
    }
    void Update()
    {
        time += Time.smoothDeltaTime;
        TimerHolder.text = "Time: "+time;
        population = GameObject.FindGameObjectsWithTag("mob").Length;
        PopulationCounter.text = "Population: "+population;
        killcounter=0;
        foreach(GameObject mob in GameObject.FindGameObjectsWithTag("mob"))
        {
            killcounter += mob.GetComponent<Brain>().killed;
        }
        KillHolder.text = "Killed: "+killcounter;
        SurvivalCounter.text = "Survival Rate: "+((accepted/(population+killcounter))*100)+"%";
        if(call)
        {
            StartCoroutine(Timer());
            call = false;
        }
    }
    IEnumerator Timer()
    {
        yield return new WaitForSeconds(10);
        Debug.Log("Pushed");
        discarded = 0;
        accepted = 0;
        time =0;
        Compile();
    }
}