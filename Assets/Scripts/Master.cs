using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Sprites;
public class Master : MonoBehaviour
{
    public int playerCount=0;
    public GameObject MobPrefab;

    // Start is called before the first frame update
    void Start()
    {
        for(int i=0;i<1000;i++)
        {
            Instantiate<GameObject>(MobPrefab).transform.position = new Vector3(Random.Range(-62,62),Random.Range(-62,62),-0.2f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        playerCount = GameObject.FindGameObjectsWithTag("mob").Length;
    }
}
